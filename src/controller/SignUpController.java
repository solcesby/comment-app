package controller;

import entity.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import service.DatabaseService;
import service.UserService;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static entity.enums.Role.USER;
import static service.DatabaseService.closeConnection;
import static utility.ImageLink.TITLE_IMAGE_LINK;

public class SignUpController {
    private static final String SELECT_ID_QUERY = "SELECT MAX(Id) FROM Users";

    DatabaseService dbService = new DatabaseService();
    UserService userService = new UserService();

    @FXML
    private PasswordField passwordField;

    @FXML
    private PasswordField passwordRepeatField;

    @FXML
    private TextField firstNameField;

    @FXML
    private TextField secondNameField;

    @FXML
    private TextField loginField;

    @FXML
    private Button signUpButton;

    @FXML
    void initialize() {
        signUpButton.setOnAction(actionEvent -> {
            boolean flag;

            String firstName = firstNameField.getText();
            String secondName = secondNameField.getText();
            String login = loginField.getText();
            String password = passwordField.getText();
            String passwordRepeat = passwordRepeatField.getText();

            User user = new User(getIdFromDB(), firstName, secondName, login, password);
            user.setRole(USER);

            flag = userService.signUp(user, passwordRepeat);

            if (flag) {
                signUpButton.getScene().getWindow().hide();

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/view/signIn.fxml"));

                try {
                    loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Parent root = loader.getRoot();
                Stage stage = new Stage();
                stage.getIcons().add(new Image(TITLE_IMAGE_LINK));
                stage.setTitle("Comment App");
                stage.setScene(new Scene(root));
                stage.show();
            }
        });

    }

    private Long getIdFromDB() {
        long id = 0L;

        try {
            Statement statement = dbService.getConnection().createStatement();

            ResultSet resSet = statement.executeQuery(SELECT_ID_QUERY);

            if (resSet.next()) {
                id = resSet.getLong(1);
                System.out.println(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }

        return ++id;
    }
}