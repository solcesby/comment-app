package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import service.CommentService;

import java.io.IOException;

import static service.UserService.currentUser;
import static utility.ImageLink.TITLE_IMAGE_LINK;

public class SignInController {
    CommentService commentService = new CommentService();

    @FXML
    private Button updateButton;

    @FXML
    private TextArea allCommentsArea;

    @FXML
    private TextArea commentField;

    @FXML
    private Button leaveCommentButton;

    @FXML
    private Label welcomeLabel;

    @FXML
    private Button signOutButton;

    @FXML
    void initialize() {
        welcomeLabel.setText(welcomeLabel.getText() + currentUser.getFirstName() + '!');

        allCommentsArea.setText(commentService.getComments());
        allCommentsArea.positionCaret(allCommentsArea.getLength());

        signOutButton.setOnAction(actionEvent -> {
            signOutButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/main.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.getIcons().add(new Image(TITLE_IMAGE_LINK));
            stage.setTitle("Comment App");
            stage.setScene(new Scene(root));
            stage.show();
        });

        leaveCommentButton.setOnAction(actionEvent -> {
            String comment = commentField.getText();

            commentField.setText("");

            commentService.leaveComment(comment);

            allCommentsArea.setText(commentService.getComments());
            allCommentsArea.positionCaret(allCommentsArea.getLength());
        });

        updateButton.setOnAction(actionEvent -> {
            allCommentsArea.setText(commentService.getComments());
            allCommentsArea.positionCaret(allCommentsArea.getLength());
        });
    }
}

