package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import service.UserService;

import java.io.IOException;

import static utility.ImageLink.TITLE_IMAGE_LINK;

public class MainController {
    UserService userService = new UserService();

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField loginField;

    @FXML
    private Button signInButton;

    @FXML
    private Button signUpButton;

    @FXML
    void initialize() {

        signUpButton.setOnAction(actionEvent -> {
            signUpButton.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/signUp.fxml"));

            try {
                loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.getIcons().add(new Image(TITLE_IMAGE_LINK));
            stage.setTitle("Регистрация");
            stage.setScene(new Scene(root));
            stage.show();

        });

        signInButton.setOnAction(actionEvent -> {
            String email = loginField.getText();
            String password = passwordField.getText();

            if (!email.equals("") && !password.equals("")) {
                if (userService.signIn(email, password)) {
                    signInButton.getScene().getWindow().hide();

                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/view/signIn.fxml"));

                    try {
                        loader.load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Parent root = loader.getRoot();
                    Stage stage = new Stage();
                    stage.getIcons().add(new Image(TITLE_IMAGE_LINK));
                    stage.setTitle("Comment App");
                    stage.setScene(new Scene(root));
                    stage.show();
                } else {
                    System.out.println("Incorrect Email or Password");
                }
            } else {
                System.out.println("Login or password is empty");
            }
        });
    }
}

