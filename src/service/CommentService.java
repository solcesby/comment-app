package service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static service.DatabaseService.closeConnection;
import static service.UserService.currentUser;

public class CommentService {
    private static final String INSERT_COMMENT_QUERY = "INSERT INTO Comments(Date, Name, Comment) VALUES(?,?,?)";
    private static final String SELECT_COMMENT_QUERY = "SELECT * FROM Comments ORDER BY id";

    DatabaseService dbService = new DatabaseService();

    public void leaveComment(String comment) {

        LocalDateTime dateTime = now();
        DateTimeFormatter myFormatObj = ofPattern("yyyy-MM-dd HH:mm:ss");
        String sqlDate = dateTime.format(myFormatObj);

        try {
            PreparedStatement prepSt = dbService.getConnection().prepareStatement(INSERT_COMMENT_QUERY);
            prepSt.setString(1, sqlDate);
            prepSt.setString(2, currentUser.getFirstName());
            prepSt.setString(3, comment);

            prepSt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }

    }

    public String getComments() {
        StringBuilder result = new StringBuilder();

        try {
            Statement statement = dbService.getConnection().createStatement();

            ResultSet resSet = statement.executeQuery(SELECT_COMMENT_QUERY);

            while (resSet.next()) {

                result.append(resSet.getString("Date")).append(" | ");
                result.append(resSet.getString("Name")).append(": ");
                result.append(resSet.getString("Comment")).append("\n");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }

        return result.toString();
    }
}

