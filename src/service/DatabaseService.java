package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseService {

    // JDBC URL, username and password of MySQL server
    private static final String DB_URL = "jdbc:mysql://sql7.freemysqlhosting.net:3306/sql7324867";
    public static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String USER = "sql7324867";
    private static final String PASSWORD = "uPfmvVgEUA";

    // JDBC variable for opening and managing connection
    private static Connection connection;

    public Connection getConnection() {
        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
            System.out.println("Successfully connected to DB!");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("Driver not found!");
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
            System.out.println("Connection failed...");
        }
        return connection;
    }

    public static void closeConnection() {
        try {
            connection.close();
            System.out.println("Connection closed!");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Connection closing failed...");
        }
    }

}

