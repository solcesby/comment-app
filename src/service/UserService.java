package service;

import entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static service.DatabaseService.closeConnection;

public class UserService {

    private static final String INSERT_USER_QUERY = "INSERT INTO Users(FirstName, SecondName, Login, Password, Role) VALUES(?,?,?,?,?)";
    private static final String SELECT_USER_QUERY = "SELECT * FROM Users WHERE Login=?";

    public static User currentUser;

    DatabaseService dbService = new DatabaseService();

    public boolean signUp(User user, String passwordRepeat) {

        if (!checkIfLoginIsTaken(user.getLogin())) {
            if (user.getPassword().equals("") || !user.getPassword().equals(passwordRepeat)) {
                System.out.println("Passwords don't match or empty!");
                return false;
            }

            try {
                PreparedStatement prepSt = dbService.getConnection().prepareStatement(INSERT_USER_QUERY);

                prepSt.setString(1, user.getFirstName());
                prepSt.setString(2, user.getSecondName());
                prepSt.setString(3, user.getLogin());
                prepSt.setString(4, user.getPassword());
                prepSt.setString(5, String.valueOf(user.getRole()));

                prepSt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeConnection();
            }

            currentUser = user;
            return true;
        } else {
            return false;
        }
    }

    public boolean signIn(String login, String password) {
        boolean toReturn = false;

        try {
            PreparedStatement prepSt = dbService.getConnection().prepareStatement(SELECT_USER_QUERY);
            prepSt.setString(1, login);

            ResultSet resSet = prepSt.executeQuery();

            if (resSet.next()) {
                long id = resSet.getLong("Id");
                String firstName = resSet.getString("FirstName");
                String secondName = resSet.getString("SecondName");
                String dbLogin = resSet.getString("Login");
                String dbPassword = resSet.getString("Password");

                if (dbPassword.equals(password)) {
                    currentUser = new User(id, firstName, secondName, dbLogin, dbPassword);
                    toReturn = true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return toReturn;
    }

    private boolean checkIfLoginIsTaken(String login) {
        try {
            PreparedStatement prepSt = dbService.getConnection().prepareStatement(SELECT_USER_QUERY);
            prepSt.setString(1, login);

            ResultSet resSet = prepSt.executeQuery();

            if (resSet.next()) {
                System.out.println("Login is already registered!");
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return false;
    }
}
